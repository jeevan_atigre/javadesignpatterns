package com.servicenow;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import com.servicenow.model.UserProfile;
import com.servicenow.service.UserProfileService;
	
@Transactional
@SpringBootTest
@RunWith(SpringRunner.class)
public class ServiceNowDemoApplicationTest {

	@Autowired
	private UserProfileService userService;

	@Test
	public void testAddUserProfile() {
		UserProfile userProfile = new UserProfile();
		userProfile.setFirstName("Sachin");
		userProfile.setLastName("Patil");
		userProfile.setDateModified(new Timestamp(0));
		userProfile.setDateCreated(new Timestamp(0));
		userProfile.setCreatedBy(1l);
		try {
			userService.saveUser(userProfile);
			assertThat("User saved!");
		} catch (AssertionError e) {
			assertThat("User not saved!");
		} catch (Exception e) {
		}
	}

	@Test
	public void testGetUserProfile() {
		Optional<UserProfile> userProfile = null;
		Long userId = 1l;
		try {
			userProfile = userService.getUserProfile(userId);
			assertThat("user found!");
		} catch (AssertionError e) {
			Assert.isNull(userProfile, "user not found");
		} catch (Exception e) {
		}

	}

	@Test
	public void testUpdateUserProfile() {
		UserProfile userProfile = new UserProfile();
		userProfile.setFirstName("Sachin");
		userProfile.setLastName("Jadhav");
		userProfile.setUserId(1l);
		userProfile.setDateModified(new Timestamp(0));
		userProfile.setDateCreated(new Timestamp(0));
		userProfile.setCreatedBy(1l);
		try {
			userService.updateUser(userProfile);
			assertThat("User updated!");
		} catch (AssertionError e) {
			assertThat("User not updated!");
		} catch (Exception e) {
		}
	}

	@Test
	public void testDeleteUserProfile() {
		Long userId = 1l;
		try {
			userService.deleteUserProfiel(userId);
			assertThat("user deleted!");
		} catch (AssertionError e) {
			assertThat("user not deleted!");
		} catch (Exception e) {
		}

	}

}
