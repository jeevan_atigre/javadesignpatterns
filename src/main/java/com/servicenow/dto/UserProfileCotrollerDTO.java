package com.servicenow.dto;

public class UserProfileCotrollerDTO {

	String sortBy;
	String direction;
	String searchString;
	int pageNo;
	int offset;

	public UserProfileCotrollerDTO(){
		
	}
	
	public UserProfileCotrollerDTO(String sortBy, String direction, String searchString, int pageNo, int offset) {
		super();
		this.sortBy = sortBy;
		this.direction = direction;
		this.searchString = searchString;
		this.pageNo = pageNo;
		this.offset = offset;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

}
