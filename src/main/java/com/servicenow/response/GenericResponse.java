package com.servicenow.response;

public class GenericResponse<T> {

	private String message;
	private String status;
	private int statusCode;
	private T data;

	public GenericResponse() {
	}

	public GenericResponse(String message, T data, String status, int statusCode) {
		super();
		this.message = message;
		this.data = data;
		this.status = status;
		this.statusCode = statusCode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}
