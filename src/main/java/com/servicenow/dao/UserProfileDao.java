package com.servicenow.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.servicenow.model.UserProfile;

public interface UserProfileDao {

	UserProfile importUserCheck(String fName, String lName, Date dob);
	
	boolean bulkSaveUserProfile(List<UserProfile> userProfileList);
	
	void saveOrUpdate(UserProfile user);

	Page<UserProfile> findAllUser(Pageable pageable);
	
}
