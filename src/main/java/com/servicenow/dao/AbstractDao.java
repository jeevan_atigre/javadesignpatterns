package com.servicenow.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

public abstract class AbstractDao<PK extends Serializable, T> {

	private final Class<T> persistentClass;
	
	private static final Logger logger = Logger.getLogger(AbstractDao.class); 
	
	private int batchSize = 5000;
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
	}

	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	protected T getByKey(PK key) {
		T t;
		try {
			t = (T) entityManager.find(persistentClass, key);
		} catch (Exception e) {
			throw new RuntimeException("Error: " + e.getStackTrace());
		}
		return t;
	}

	protected void persist(T entity) {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException("Error: " + e.getStackTrace());
		}

	}

	protected void update(T entity) {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException("Error: " + e.getStackTrace());
		}
	}
	
	protected void saveOrUpdate(T entity) {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			throw new RuntimeException("Error: " + e.getStackTrace());
		}
	}

	protected void delete(T entity) {
		try {
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException("Error: " + e.getStackTrace());
		}
	}

	public boolean bulkSave(List<T> entities) {
	  int i = 0;
	  try {
		  for (T t : entities) {
			  entityManager.persist(entityManager.contains(t) ? t : entityManager.merge(t));
		    i++;
		    if (i % batchSize == 0) {
		      entityManager.flush();
		      entityManager.clear();
		    }
		  }
	  } catch (Exception e) {
			logger.error(e);
			return false;
	  }
	  return true;
	}
	
	protected void deleteDetatchedEntity(T entity) {
		try {
			entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException("Error: " + e.getStackTrace());
		}
	}
}
