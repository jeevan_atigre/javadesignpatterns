package com.servicenow.daoImpl;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.servicenow.dao.AbstractDao;
import com.servicenow.dao.UserProfileDao;
import com.servicenow.model.UserProfile;

@Transactional
@Repository("UserProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Long, UserProfile> implements UserProfileDao {

	private static final Logger logger = Logger.getLogger(UserProfileDaoImpl.class);

	@Override
	public UserProfile importUserCheck(String fName, String lName, Date dob) {
		UserProfile user = null;
		try {
			if (fName != null && lName != null && dob != null) {
				Query query = getEntityManager().createQuery(
						"SELECT u FROM UserProfile u WHERE u.firstName = :fName and u.lastName = :lName and u.dob = :dob");
				query.setParameter("fName", fName);
				query.setParameter("lName", lName);
				query.setParameter("dob", dob);
				List<UserProfile> list = query.getResultList();
				if (!list.isEmpty()) {
					user = list.get(0);
				}
			}
		} catch (Exception e) {
			logger.error("*** Exception while getting patient in importPatientCheck ***", e);
		}
		return user;
	}

	@Override
	public boolean bulkSaveUserProfile(List<UserProfile> userProfileList) {
		boolean bulkResult = false;
		try {
			bulkResult = bulkSave(userProfileList);
			if (!bulkResult)
				throw new Exception();
		} catch (Exception e) {
			logger.error(
					"*** Exceptiton while saving user profiles in bulk in bulkSaveUserProfile in UserProfileDaoImpl ***");
		}
		return bulkResult;
	}

	@Override
	public void saveOrUpdate(UserProfile user) {
		try {
			saveOrUpdate(user);
		} catch (Exception e) {
			logger.error("*** Exceptiton while saving user profile in saveOrUpdate in UserProfileDaoImpl ***");
		}
	}

	@Override
	public Page<UserProfile> findAllUser(Pageable pageable) {

		/*
		 * Integer offset = (int) pageable.getOffset(); Integer page = (int)
		 * pageable.getPageNumber();
		 * 
		 * 
		 * String countQuery =
		 * "SELECT COUNT(uf.USER_ID) FROM CS_USER_PROFILE uf WHERE uf.USER_ID NOT IN (SELECT pr.USER_ID FROM CS_PROVIDERS pr) AND  uf.USER_ID NOT IN (SELECT p.USER_ID FROM CS_PATIENTS p)"
		 * ; Query query = getEntityManager().createNativeQuery(countQuery.toString());
		 * 
		 * int count = (Integer) query.getSingleResult();
		 * 
		 * 
		 * String strquery =
		 * "SELECT * FROM CS_USER_PROFILE uf WHERE uf.USER_ID NOT IN (SELECT pr.USER_ID FROM CS_PROVIDERS pr) AND  uf.USER_ID NOT IN (SELECT p.USER_ID FROM CS_PATIENTS p)"
		 * ;
		 * 
		 * query.setFirstResult(offset * (page - 1)); query.setMaxResults(offset);
		 * return query.getResultList();
		 */
		return null;
	}

}
