package com.servicenow.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.servicenow.dto.UserProfileDTO;
import com.servicenow.model.UserProfile;

@Mapper
public interface UserProfileMapper extends EntityMapper<UserProfile, UserProfileDTO> {

	UserProfileMapper INSTANCE = Mappers.getMapper(UserProfileMapper.class);

	@Mapping(ignore = true, target = "password")
	UserProfileDTO toDto(UserProfile entity);

	UserProfile toEntity(UserProfileDTO dto);

	List<UserProfileDTO> toList(List<UserProfile> profiles);
}
