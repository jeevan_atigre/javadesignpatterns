package com.servicenow.mapper;

import org.springframework.data.domain.Page;

public interface EntityMapper<E, D> {

	D toDto(E source);

	E toEntity(D destination);

//	List<E> toEntityList(List<D> dtoList);
//
//	List<D> toDtoList(List<E> entityList);

	default Page<D> toDtoPageList(Page<E> page) {
		return page.map(this::toDto);
	}

	default Page<E> toEntityPageList(Page<D> page) {
		return page.map(this::toEntity);
	}
}
