package com.servicenow.service;

import java.io.IOException;

import org.json.JSONObject;

public interface ApplicationService {

	public String getIncidentTableRecords() throws IOException;
	
	public String postIncidentTableRecords(String postJSONString) throws IOException;
	
}
