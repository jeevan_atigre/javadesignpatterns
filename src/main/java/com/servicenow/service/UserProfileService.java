package com.servicenow.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.servicenow.dto.UserProfileCotrollerDTO;
import com.servicenow.model.UserProfile;

@Service
public interface UserProfileService {

	UserProfile saveUser(UserProfile user) throws Exception;

	Optional<UserProfile> getUserProfile(Long userProfileId) throws Exception;

	UserProfile updateUser(UserProfile user) throws Exception;

	void deleteUserProfiel(Long userProfileId) throws Exception;
	
	void saveOrUpdate(UserProfile user);

}
