package com.servicenow.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/*@Configuration
@EnableSwagger2*/
//@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

	@Bean
	public Docket api() {

		// Adding Header
		ParameterBuilder aParameterBuilder = new ParameterBuilder();
		aParameterBuilder.name("Authorization") // name of header
				.modelRef(new ModelRef("string")).parameterType("header") // type - header
				.description("Replace <access_token> with actual value").defaultValue("Bearer <access_token>")
				.required(true) // for compulsory
				.build();
		List<Parameter> aParameters = new ArrayList<>();
		aParameters.add(aParameterBuilder.build());

		return new Docket(DocumentationType.SWAGGER_2).groupName("APIs").select()
				.apis(RequestHandlerSelectors.basePackage("com.servicenow.controller"))
				.paths(PathSelectors.regex("/api/.*")).build().globalOperationParameters(aParameters);
	}

	@Bean
	public Docket oauthApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("Authentication").select()
				.apis(RequestHandlerSelectors.basePackage("com.servicenow.controller"))
				.paths(PathSelectors.regex("/oauth/.*")).build();
	}
}