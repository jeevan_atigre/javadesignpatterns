package com.servicenow.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.servicenow.model.UserProfile;

@Repository("UserRepository")
public interface UserRepository extends JpaRepository<UserProfile, Long> {

	UserProfile findByUsername(String username);

	
	@Query(value = "SELECT uf FROM UserProfile uf WHERE uf.userId NOT IN (SELECT pr.userProfile.userId FROM Provider pr) AND  uf.userId NOT IN (SELECT p.userProfile.userId FROM Patient p) ORDER BY ?#{#pageable} ", 
			countQuery = "SELECT COUNT(uf.userId) FROM UserProfile uf WHERE uf.userId NOT IN (SELECT pr.userProfile.userId FROM Provider pr) AND  uf.userId NOT IN (SELECT p.userProfile.userId FROM Patient p)", 
			nativeQuery = true)
	Page<UserProfile> findAllUsersRepo(Pageable pageable);
	  
	  Page<UserProfile> findByFirstNameIgnoreCaseContainingOrUsernameIgnoreCaseContaining(String FirstName,
			String Username, Pageable pageable);

}