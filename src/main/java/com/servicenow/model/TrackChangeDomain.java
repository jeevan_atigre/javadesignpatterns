package com.servicenow.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import io.swagger.annotations.ApiModelProperty;

@MappedSuperclass
public abstract class TrackChangeDomain implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(hidden = true)
	@Column(name = "DATE_CREATED", updatable = false)
	private Timestamp dateCreated;

	@ApiModelProperty(hidden = true)
	@Column(name = "CREATED_BY", updatable = false)
	private Long createdBy;

	@ApiModelProperty(hidden = true)
	@Column(name = "DATE_MODIFIED")
	private Timestamp dateModified;

	@ApiModelProperty(hidden = true)
	@Column(name = "MODIFIED_BY")
	private Long modifiedBy;

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getDateModified() {
		return dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@PrePersist
	protected void onCreate() {
		if (dateCreated == null) {
			dateCreated = new Timestamp(System.currentTimeMillis());
			dateModified = dateCreated;
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
			UserProfile user = (UserProfile) auth.getPrincipal();
			createdBy = user.getUserId();
			modifiedBy = createdBy;
		} else {
			createdBy = null;
			modifiedBy = createdBy;
		}
	}

	@PreUpdate
	protected void onUpdate() {
		dateModified = new Timestamp(System.currentTimeMillis());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
			UserProfile user = (UserProfile) auth.getPrincipal();
			createdBy = user.getUserId();
			modifiedBy = createdBy;
		} else {
			createdBy = null;
			modifiedBy = createdBy;
		}
	}

}
