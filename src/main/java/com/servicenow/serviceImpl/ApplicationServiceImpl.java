package com.servicenow.serviceImpl;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.servicenow.service.ApplicationService;

@Service("ApplicationService")
@Transactional
public class ApplicationServiceImpl implements ApplicationService{

	@Override
	public String getIncidentTableRecords() {
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(new HttpHost("dev81230.service-now.com")),
                new UsernamePasswordCredentials("admin", "Admin@123"));
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build();
        StringBuilder incidentTableString = null;
        try {
            HttpGet httpget = new HttpGet("https://dev81230.service-now.com/api/now/table/incident");
            httpget.setHeader("Accept", "application/json");
            System.out.println("Executing request " + httpget.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httpget);
            incidentTableString = new StringBuilder(EntityUtils.toString(response.getEntity()));
            System.out.println("Incident table string: " + incidentTableString);
        } catch(Exception e) {
        	System.out.println(e);
        }
		return incidentTableString.toString();
	}

	@Override
	public String postIncidentTableRecords(String postJSONString) throws IOException {
 		CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(new HttpHost("dev81230.service-now.com")),
                new UsernamePasswordCredentials("admin", "Admin@123"));
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build();
        StringBuilder incidentTableString = null;
	    try {
	    	HttpPost httpPost = new HttpPost("https://dev81230.service-now.com/api/now/table/incident");
		 		httpPost.setHeader("Accept", "application/json");
		 		httpPost.setHeader("Content-Type", "application/json");
		        HttpEntity entity = new ByteArrayEntity(postJSONString.getBytes("utf-8"));
		 		httpPost.setEntity(entity);
		        
	        System.out.println("Executing request " + httpPost.getRequestLine());
	        CloseableHttpResponse response = httpclient.execute(httpPost);
	        incidentTableString = new StringBuilder(EntityUtils.toString(response.getEntity()));
	        System.out.println("Updated incident table string: " + incidentTableString);
	    } finally {
	        httpclient.close();
	    }
	    return incidentTableString.toString();
	}
}
