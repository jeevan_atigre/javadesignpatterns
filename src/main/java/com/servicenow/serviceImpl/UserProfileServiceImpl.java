package com.servicenow.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.servicenow.dao.UserProfileDao;
import com.servicenow.dto.UserProfileCotrollerDTO;
import com.servicenow.model.UserProfile;
import com.servicenow.repository.UserRepository;
import com.servicenow.service.UserProfileService;

@Service("UserProfileService")
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	private UserProfileDao dao;

	@Autowired
	private UserRepository userRepository;

	
	@Override
	public UserProfile saveUser(UserProfile user) throws Exception {
		user = userRepository.save(user);
		return user;
	}

	@Override
	public Optional<UserProfile> getUserProfile(Long userProfileId) throws Exception {
		Optional<UserProfile> user = null;
		user = userRepository.findById(userProfileId);
		return user;
	}

	@Override
	public UserProfile updateUser(UserProfile user) throws Exception {
		user = userRepository.save(user);
		return user;
	}

	@Override
	public void deleteUserProfiel(Long userProfileId) throws Exception {
		userRepository.deleteById(userProfileId);
	}

	@Override
	public void saveOrUpdate(UserProfile user) {
		dao.saveOrUpdate(user);
	}

}
