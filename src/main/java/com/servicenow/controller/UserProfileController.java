package com.servicenow.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.servicenow.dto.UserProfileDTO;
import com.servicenow.mapper.UserProfileMapper;
import com.servicenow.model.UserProfile;
import com.servicenow.response.GenericResponse;
import com.servicenow.service.UserProfileService;
import com.servicenow.util.ResponseStatus;
import com.servicenow.util.ResponseStatusConstants;

@RestController
@RequestMapping("/api")
public class UserProfileController {

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	@Qualifier("passwordEncoder")
	PasswordEncoder passwordEncoder;

	private static final Logger logger = Logger.getLogger(UserProfileController.class);

	@RequestMapping(value = "/saveUserProfile", method = RequestMethod.POST)
	public GenericResponse<?> saveUserProfiles(@Valid @RequestBody UserProfileDTO userProfileDTO, BindingResult bindingResult) {
		GenericResponse<?> genericResponse = new GenericResponse<>();
		
		if (bindingResult.hasErrors()) {
			String validationError = "";
			for (ObjectError error : bindingResult.getAllErrors()) {
				validationError += ((FieldError) error).getField();
				validationError += " " + (error.getDefaultMessage()) + ";";
			}
			genericResponse.setMessage(validationError);
			genericResponse.setStatus(ResponseStatus.dataInvalid.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.dataInvalid.getValue());
			return genericResponse;
		}
		
		try {
			UserProfile userProfile = UserProfileMapper.INSTANCE.toEntity(userProfileDTO);
			userProfile.setPassword(passwordEncoder.encode(userProfile.getPassword()));
			userProfileService.saveUser(userProfile);
			genericResponse.setMessage("UserProfile Saved!");
			genericResponse.setStatus(ResponseStatus.success.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
			return genericResponse;
		} catch (Exception e) {
			logger.error("*****exception in UserProfileController.saveUserProfiles*****", e);
			genericResponse.setMessage("Failed to save UserProfile!");
			genericResponse.setStatus(ResponseStatus.error.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.error.getValue());
			return genericResponse;
		}
	}

	@RequestMapping(value = "/updateUserProfile", method = RequestMethod.PUT)
	public GenericResponse<?> updateUserProfiles(@Valid @RequestBody UserProfileDTO userProfileDTO, BindingResult bindingResult) {
		GenericResponse<?> genericResponse = new GenericResponse<>();
	
		if (bindingResult.hasErrors()) {
			String validationError = "";
			for (ObjectError error : bindingResult.getAllErrors()) {
				validationError += ((FieldError) error).getField();
				validationError += " " + (error.getDefaultMessage()) + ";";
			}
			genericResponse.setMessage(validationError);
			genericResponse.setStatus(ResponseStatus.dataInvalid.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.dataInvalid.getValue());
			return genericResponse;
		}
		
		try {
			UserProfile userProfile = UserProfileMapper.INSTANCE.toEntity(userProfileDTO);
			if (userProfile != null) {
					userProfile.setPassword(
							userProfileService.getUserProfile(userProfile.getUserId()).get().getPassword());
				userProfileService.updateUser(userProfile);
				genericResponse.setMessage("UserProfile Saved!");
				genericResponse.setStatus(ResponseStatus.success.toString());
				genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
				return genericResponse;
			} else {
				userProfileService.updateUser(userProfile);
				genericResponse.setMessage("UserProfile not Saved!");
				genericResponse.setStatus(ResponseStatus.success.toString());
				genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
				return genericResponse;
			}
		} catch (Exception e) {
			logger.error("*****exception in UserProfileController.updateUserProfiles*****", e);
			genericResponse.setMessage("error!");
			genericResponse.setStatus(ResponseStatus.error.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.error.getValue());
			return genericResponse;
		}
	}

	@RequestMapping(value = "/getUserProfile/{userProfileId}", method = RequestMethod.GET)
	public GenericResponse<Object> getUserProfile(@PathVariable("userProfileId") Long userProfileId) {
		GenericResponse<Object> genericResponse = new GenericResponse<>();
		Optional<UserProfile> userProfile = null;
		try {
			userProfile = userProfileService.getUserProfile(userProfileId);
			UserProfileDTO dto = UserProfileMapper.INSTANCE.toDto(userProfile.get());
			if (userProfile.isPresent()) {
				genericResponse.setMessage("User found!");
				genericResponse.setStatus(ResponseStatus.success.toString());
				genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
				genericResponse.setData(dto);
			} else {
				genericResponse.setMessage("user not found!");
				genericResponse.setStatus(ResponseStatus.success.toString());
				genericResponse.setStatusCode(ResponseStatusConstants.dataNotExists.getValue());
			}
			return genericResponse;
		} catch (Exception e) {
			logger.error("*****exception in UserProfileController.getUserProfile*****", e);
			genericResponse.setMessage("error!");
			genericResponse.setStatus(ResponseStatus.error.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.error.getValue());
			return genericResponse;
		}
	}

	@RequestMapping(value = "/deleteUserProfile/{userProfileId}", method = RequestMethod.DELETE)
	public GenericResponse<?> deleteUserProfile(@PathVariable("userProfileId") Long userProfileId) {
		GenericResponse<?> genericResponse = new GenericResponse<>();
		try {
			userProfileService.deleteUserProfiel(userProfileId);
			genericResponse.setMessage("UserProfile deleted!");
			genericResponse.setStatus(ResponseStatus.success.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
			return genericResponse;
		} catch (EmptyResultDataAccessException em) {
			logger.error("*****exception in UserProfileController.deleteUserProfile*****", em);
			genericResponse.setMessage("UserProfile with given id not found!");
			genericResponse.setStatus(ResponseStatus.success.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.dataNotExists.getValue());
			return genericResponse;
		} catch (Exception e) {
			logger.error("*****exception in UserProfileController.deleteUserProfile*****", e);
			genericResponse.setMessage("error!");
			genericResponse.setStatus(ResponseStatus.error.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.error.getValue());
			return genericResponse;
		}
	}

}
