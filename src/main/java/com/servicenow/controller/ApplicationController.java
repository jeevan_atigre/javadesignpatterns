package com.servicenow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.servicenow.response.GenericResponse;
import com.servicenow.service.ApplicationService;
import com.servicenow.util.ResponseStatus;
import com.servicenow.util.ResponseStatusConstants;

@RestController
@RequestMapping("/servicenow-test")
public class ApplicationController {
	
	@Autowired
	private ApplicationService applicationService;
	
	@RequestMapping(value = "/incident-table-details", method = RequestMethod.GET)
	public GenericResponse<Object> getIncidentTableDetails() {
		GenericResponse<Object> genericResponse = new GenericResponse<>();
		String incidentData = null;
		try {
			incidentData = applicationService.getIncidentTableRecords();
			genericResponse.setMessage("Data found!");
			genericResponse.setStatus(ResponseStatus.success.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
			genericResponse.setData(incidentData);
			return genericResponse;
		} catch (Exception e) {
			genericResponse.setMessage("error!");
			genericResponse.setStatus(ResponseStatus.error.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.error.getValue());
			return genericResponse;
		}
	}
	
	@RequestMapping(value = "/incident-table-details", method = RequestMethod.POST)
	public GenericResponse<Object> postIncidentTableDetails(@RequestBody String postJSONString) {
		GenericResponse<Object> genericResponse = new GenericResponse<>();
		String incidentData = null;
		try {
			incidentData = applicationService.postIncidentTableRecords(postJSONString);
			genericResponse.setMessage("Data inserted!");
			genericResponse.setStatus(ResponseStatus.success.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.success.getValue());
			genericResponse.setData(incidentData);
			return genericResponse;
		} catch (Exception e) {
			genericResponse.setMessage("error!");
			genericResponse.setStatus(ResponseStatus.error.toString());
			genericResponse.setStatusCode(ResponseStatusConstants.error.getValue());
			return genericResponse;
		}
	}

}
