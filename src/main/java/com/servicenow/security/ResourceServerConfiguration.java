package com.servicenow.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

//TODO- REMOVE FILE
//@Configuration
//@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource-server-rest-api";
//	private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
//	private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
//	private static final String SECURED_PATTERN = "/secured/**";

	@Autowired
	@Qualifier("tokenStore")
	TokenStore tokenStore;
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).tokenStore(tokenStore);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
//		http.csrf().disable().cors().disable().anonymous().disable().requestMatchers().antMatchers(SECURED_PATTERN)
//				.and().authorizeRequests().antMatchers(HttpMethod.POST, SECURED_PATTERN).access(SECURED_WRITE_SCOPE)
//				.anyRequest().access(SECURED_READ_SCOPE);
		
		http.csrf().disable().cors().disable()
		.authorizeRequests().antMatchers("/swagger-ui.html", "/webjars/**", "/v2/**", "/swagger-resources/**").anonymous()
		.and()
		.authorizeRequests().antMatchers("/**").authenticated()
        .antMatchers("/admin/**").access("hasRole('ADMIN')")
        .antMatchers("/user/**").access("hasRole('USER')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}
}
